
let leftPosition = 0;
let topPosition = (window.innerHeight-50);
let goTo = true;
let backFrom = false;

let time = setInterval(moving, 10);

function moving() 
{
    const movingBox = document.getElementById("movingBox")

    if(leftPosition >= (window.innerWidth-50)) {
        goTo = false;
        backFrom = true;
    } else if(leftPosition <= 0) {
        goTo = true;
        backFrom = false;
    }

    if(goTo) {
        leftPosition = (leftPosition + 1*2.3);
        topPosition = (topPosition - 1);
    } else if(backFrom) {
        leftPosition = (leftPosition - 1*2.3);
        topPosition = (topPosition + 1);
    }

    movingBox.style.top = topPosition + "px"; 
    movingBox.style.left = leftPosition + "px";

}
